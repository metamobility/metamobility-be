import dbConnect from '../../schema/db';
import Model from '../../schema/model';
import allowCors from '../allowCors';

const handler = async (req, res) => {
  const { id } = req.query;
  const db = await dbConnect();

  const response = await Model.findOne({ _id: id })
    .select(
      '-_id -__v -characteristics.lat -characteristics.lon -characteristics.latitude -characteristics.longitude'
    )
    .exec();
  await db.connection.close();

  res.status(200).json(response);
};
module.exports = allowCors(handler);
