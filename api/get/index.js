import dbConnect from '../../schema/db';
import Model from '../../schema/model';
import allowCors from '../allowCors';

const handler = async (_, res) => {
  const db = await dbConnect();
  try {
    const data = await Model.find()
      .select(
        'characteristics.lat characteristics.lon characteristics.latitude characteristics.longitude type _id'
      )
      .exec();

    res.status(200).send(data);
  } catch (e) {
    res.status(400).send({ error: `ERROR: ${e}` });
  }

  await db.connection.close();
};

module.exports = allowCors(handler);
