const fs = require('fs');

const generalFile = fs
  .readFileSync('./auto.txt', { encoding: 'utf-8' })
  .toString();

const output = [];

generalFile.split(',\r\n').forEach((element, i) => {
  const entry = element.split(':');
  output.push(entry[0] + ': ' + `entry[${i}]`);
});
console.log(output.join(',\r\n'));
