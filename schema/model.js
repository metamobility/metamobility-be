const mongoose = require('mongoose');

const Model = mongoose.model(
  'metamobility',
  mongoose.Schema({
    type: mongoose.Schema.Types.String,
    characteristics: {
      type: mongoose.Schema.Types.Map,
      of: String,
    },
  })
);

module.exports = Model;
